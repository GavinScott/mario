#! /usr/bin/env python

import pygame
from pygame.locals import *

from gamelib.data import *
from gamelib.sprites import *

import sys
from random import randint

PIPE_FLAG = '-p'
TURT_FLAG = '-t'
GOOM_FLAG = '-g'

def handleArgs():
    reqs = {}
    args = sys.argv
    if len(sys.argv) > 1:
        for arg in args[1:]:
            if arg[:2] in [PIPE_FLAG, TURT_FLAG, GOOM_FLAG]:
                reqs[arg[:2]] = int(arg[2:])
    return reqs 

class Level:

    def __init__(self, lvl=1):
        self.level = pygame.image.load(filepath(("levels/lvl%d.png" % lvl))).convert()
        self.x = 0
        self.y = 0
        
        for x in range(self.level.get_width()):
            Platform((x*32, (self.level.get_height() - 1)*32), "middle", False, False)
        
        spots = list(range(4, self.level.get_width(), 8))
        reqs = handleArgs()
        if len(reqs) > 0:
            for r in reqs:
                # PIPE
                if r == PIPE_FLAG:
                    y = (self.level.get_height() - 2.4)
                    selected = random.sample(spots, min(len(spots), reqs[r]))
                    print('Adding', len(selected), 'pipes')
                    for spot in selected:
                        spots.remove(spot)
                        Pipe((spot*32, y*28))
                # TURTLE
                elif r == TURT_FLAG:
                    y = (self.level.get_height() - 2.4)
                    selected = random.sample(spots, min(len(spots), reqs[r]))
                    print('Adding', len(selected), 'turtles')
                    for spot in selected:
                        spots.remove(spot)
                        Baddie((spot*32 + 2, y*32 + 4), "monster")
                # GOOMBA
                elif r == GOOM_FLAG:
                    y = (self.level.get_height() - 2.4)
                    selected = random.sample(spots, min(len(spots), reqs[r]))
                    print('Adding', len(selected), 'goombas')
                    for spot in selected:
                        spots.remove(spot)
                        Baddie((spot*32 + 2, y*32 + 4), "slub")
                   
    def get_at(self, dx, dy):
        try:
            return self.level.get_at((self.x+dx, self.y+dy))
        except:
            pass
            
    def get_size(self):
        return [self.level.get_size()[0]*32, self.level.get_size()[1]*32]
