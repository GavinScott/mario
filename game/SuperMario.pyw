#! /usr/bin/env python
import sys

HELP_TEXT = '''
DATA SOURCE:
    (no flag) : playing the game
    -c        : cached data

BOT SELETION:
    (no flag) : reactive bot
    -manual   : manual control
    -nn       : neural network bot
    -m        : mimicking bot
    -hy       : hybrid bot

LEVEL GENERATION:
    generate  : indicates to use generated of level
    -p[#]     : generates "#" pipes
    -t[#]     : generates "#" turtles
    -g[#]     : generates "#" goombas
'''

from gamelib import main
if '-h' in sys.argv:
    print(HELP_TEXT)
    exit()
main.main()
