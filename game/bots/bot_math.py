from gamelib import sprites

TILE_SIZE = 32

HAZARDS = ("black", "blueflower", "bluemonster", "bowser", "bowser-fireball",
           "cannonbullet", "flower", "monster", "monster-red", "rose", "sky",
           "slub", "slubblue", "spiker", "spikeshot", "squidge")

def isHazard(entity):
    entityName = str(entity.obj_type)

    if entityName in HAZARDS:
        return True
    return False
           
def getHazardID(entity):
    entityName = str(entity.obj_type)
    entityID = -1
    
    if entityName in HAZARDS:
      entityID = HAZARDS.index(entityName)
      
    return entityID
           
def getRelativeDistance(player, entity):
    return entity.pos[0] - player[0]

def getRelativeDistances(player, entities):
    dists = {}
    for e in entities:
        dists[e] = getRelativeDistance(player, e)
    return dists
    
def getEntitiesWithinDistance(player, entities, distance):
    li = []
    rel_dists = getRelativeDistances(player, entities)
    for e in rel_dists:
        if rel_dists[e] <= distance and abs(e.pos[1] - player[1]) < 32:
            li.append(e)
    return li
