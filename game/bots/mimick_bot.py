from bots import bot
import gamelib
import math
import nltk
import random

classifier = None

BIN_SIZE = 20
IGNORE_PLATFORMS = True # true requires more data (3 levels vs. 1)
EQUALIZE_SAMPLES = False # no noticable effect yet

def learn():
    states = bot.SAVED_STATES
    stateTypes = {}
    for state in states:
        if state[1] not in stateTypes:
            stateTypes[state[1]] = []
        stateTypes[state[1]].append(state[0])
    
    if EQUALIZE_SAMPLES:
        # deletes extras (results in same number of jumps/no jumps
        minLen = None
        for state in stateTypes:
            if minLen == None or len(stateTypes[state]) < minLen:
                minLen = len(stateTypes[state])
        for state in stateTypes:
            while len(stateTypes[state]) > minLen:
                random.shuffle(stateTypes[state])
                del stateTypes[state][0]
        states = []
        for state in stateTypes:
            for gs in stateTypes[state]:
                states.append((gs, state))
    
    # build features
    features = [(getFeatures(state[0]), state[1]) for state in states]
    print('------------------------------------------')
    for f in features:
        print(f)
    global classifier
    classifier = nltk.NaiveBayesClassifier.train(features)
    print('------------------------------------------')
    return classifier
    
def getFeatures(state):
    p = state.player
    nearest = {}
    for e in state.entities:
        if not IGNORE_PLATFORMS or e.obj_type != gamelib.sprites.Platform:
            distance = int(math.ceil((e.pos[0] - p[0]) / BIN_SIZE)) * BIN_SIZE
            if distance > 0 and ((e.obj_type not in nearest) or (distance < nearest[e.obj_type])):
                nearest[e.obj_type] = distance
            behind = str(e.obj_type) + '-behind'
            if distance < 0 and ((behind not in nearest) or (distance < nearest[behind])):
                nearest[behind] = distance
    return nearest

def getDecision(state):
    global classifier
    if classifier == None:
        learn()
    dec = classifier.classify(getFeatures(state))
    return dec
    

