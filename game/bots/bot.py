import pickle
import sys
import os

# cached data flag
CACHE = '-c'

# possible directions
DIR_LEFT = -1
DIR_RIGHT = 1
DIR_STILL = 0

# number of processed frames
N_FRAMES = 0

# to test another bot, change 'test_bot' to the new file
#import bots.reactive_bot as b
from bots.game_state import *

# BOT FLAGS
NEURAL_NET = '-nn'
MIMICK = '-m'
HYBRID = '-hy'

# manual mode
MANUAL = False

# choose bot
if NEURAL_NET in sys.argv:
    from bots import neural_network_bot as b
    print('Starting Neural Net Bot...')
elif MIMICK in sys.argv:
    from bots import mimick_bot as b
    print('Starting Mimicking Bot...')
    MANUAL = True
elif HYBRID in sys.argv:
    from bots import hybrid_bot as b
    MANUAL = True
    print('Starting Hybrid Bot...')
else:
    from bots import reactive_bot as b
    if '-manual' not in sys.argv:
        print('Starting Reactive Bot...')
    else:
        print('Starting in manual mode')
        MANUAL = True

SAVED_STATES = []
MAX_SAVED_STATES = 100000
PAUSE_FRAMES = 50
FRAMES_SINCE_CACHE = 0
DELETE_WINDOW = 15
DO_DELETION = True

def loadCache():
    import gamelib.sprites
    global SAVED_STATES
    states = []
    for fname in os.listdir('cache/'):
        if '.state' in fname:
            with open('cache/' + fname, 'rb') as f:
                states.append(pickle.load(f))
    SAVED_STATES = states
    print('loaded', len(states), 'cached states')
    return states

# switched to cached data
if CACHE in sys.argv:
    MANUAL = False
    if len(loadCache()) == 0:
        print('ERROR: no cached data')
        exit()
    

def willSwitchtoBotControl():
    return MIMICK in sys.argv or HYBRID in sys.argv

# CALLED BY THE GAME ONCE PER FRAME
# gets the bot's decision for the frame
# decisions are in the form of (LEFT/RIGHT/STILL, True/False for jump)
def getDecision(state):
    return b.getDecision(state)

# gets the current number of processed frames (for dev benefit, not the bot's)
def getFrames():
    global N_FRAMES
    return N_FRAMES
    
def needNewState():
    return FRAMES_SINCE_CACHE >= PAUSE_FRAMES

def newFrame():
    global N_FRAMES
    global FRAMES_SINCE_CACHE
    N_FRAMES += 1
    FRAMES_SINCE_CACHE += 1
    
def cache(gamestate, action):
    if len(os.listdir('cache/')) > 0:
        m = max([int(o.replace('cache/', '').replace('.state', '')) for o in os.listdir('cache/') if '.state' in o])
    else:
        m = 0
    with open('cache/' + str(m + 1) + '.state', 'wb') as f:
        pickle.dump((gamestate, action), f)

def saveState(gamestate, action):
    global FRAMES_SINCE_CACHE
    global SAVED_STATES
    # delete if last state was too soon before this one
    if DO_DELETION and FRAMES_SINCE_CACHE <= DELETE_WINDOW and len(SAVED_STATES) > 0 and SAVED_STATES[-1][1] == action:
        del SAVED_STATES[-1]
        print('deleting last state')
   
    FRAMES_SINCE_CACHE = 0
    print("SAVING STATE | action =", action)
    print()
    # cache the state
    SAVED_STATES.append((gamestate, action))
    # truncate number of saved states for performance
    SAVED_STATES = SAVED_STATES[-MAX_SAVED_STATES:]
    # cache state
    cache(gamestate, action)

def init_ref(player):
    if NEURAL_NET in sys.argv:
        b.init_bot(player)
