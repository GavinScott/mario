# an object representing the game state
class GameState():
    # player location, list of object locations
    def __init__(self, player, entities):
        self.player = player
        self.entities = entities

    def __init__(self, player, entities, level):
        self.player = player
        self.entities = entities
        self.level = level

# an object in the game (enemy, pipe, etc.)
class Entity():
    def __init__(self, pos, obj_type, speed):
        self.pos = pos
        self.obj_type = obj_type
        self.speed = speed
    
    def out(self):
        return 'ENTITY:', self.obj_type, self.pos, self.speed

    def outScaled(self):
        return 'ENTITY:', self.obj_type, int(self.pos[0] / 32), int(self.pos[1] / 32)
