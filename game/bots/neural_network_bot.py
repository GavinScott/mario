from bots import bot
from bots.game_state import *
from bots import bot_math

from gamelib import sprites

from keras.models import Sequential
from keras.models import load_model
from keras.layers import Dense
from keras.layers import Activation
from keras.utils import np_utils

import numpy
from pathlib import Path
from pprint import pprint

# evaluation options
FRAME_SKIP = 3
EPOCH = 10
BATCH_SIZE = 20

# extra
NN_MODEL_PATH = "nn_model.h5"
NN_MODEL_SAVE_SKIP = 450 # every 15 in-game seconds

# tile checking options
TILE_X_OFFSET = 1
TILE_Y_OFFSET = -2
TILE_MAX_X_DIST = 5
TILE_MAX_Y_DIST = 5
ENEMY_TILE_CHECK_DIST = 4

# heuristic options
GAME_SCORE_SCALE = 20
GET_HIT_PENALTY = -500
STALL_PENALTY = -500
JUMP_PENALTY = -4.5
STALL_MEMORY = 120

# feature constants (careful about changing these)
TOTAL_FEATURES = 142
SINGLE_ENEMY_FEATURES = 4
MAX_ENEMY_COUNT = 9

PLAYER_FEATURE_TOTAL = 6
ENEMY_FEATURE_TOTAL = 36
TILE_FEATURE_TOTAL = 100

PLAYER_FEATURE_OFFSET = 0
ENEMY_FEATURE_OFFSET = PLAYER_FEATURE_TOTAL
TILE_FEATURE_OFFSET = ENEMY_FEATURE_OFFSET + ENEMY_FEATURE_TOTAL

class NeuralNetworkBot():
   def __init__(self):
      self.player = None
      self.model = None
      self.inputData = numpy.zeros((1, 142))
      self.outputData = numpy.zeros((1, 2))
      self.lastScore = 0
      self.currentScore = 0
      self.posXHistory = numpy.zeros((1, STALL_MEMORY))
      self.frameCount = 0
      self.evaluation = None
      self.isStalled = False
      self.lastDecision = (bot.DIR_RIGHT, False)
      

   def setPlayerRef(self, player):
      self.player = player
      
      
   def initModel(self):
      # load model if it exists
      if Path(NN_MODEL_PATH).is_file():
         print("Loading NN model from file")
         self.model = load_model(NN_MODEL_PATH)
         return

      print("Setting up new NN model")
      # create model if no model file found
      self.model = Sequential()
      # 142 input features, 143 nodes (+1 for bias)
      self.model.add(Dense(output_dim=143, input_dim=142, init='he_normal', activation='tanh'))
      # middle layers for (hopefully) learning deeper features
      self.model.add(Activation("tanh"))
      self.model.add(Activation("tanh"))
      # 2 outputs (isJumping and direction)
      self.model.add(Dense(output_dim=2, init='uniform', activation='tanh'))
      # Compile model
      self.model.compile(loss='mse', optimizer='adam', metrics=['accuracy'])
      
      
   def updateIsStalled(self):
      self.isStalled = True
      
      prev = self.posXHistory[0][0]
      #print("Loop X: " + str(0) + " - " + str(prev));
      for x in range(1, STALL_MEMORY):
         #print("Loop X: " + str(x) + " - " + str(self.posXHistory[0][x]));
         if prev != self.posXHistory[0][x]:
            self.isStalled = False
            return
      
      print("***STALLED***")
      
      
   def getHeuristicScore(self):
      stallPenalty = 0
      if self.isStalled:
         stallPenalty = STALL_PENALTY
         
      if self.player.jumping:
         self.actionPenalty += JUMP_PENALTY
         
      timePenalty = -self.frameCount / 30

      return (self.player.score * GAME_SCORE_SCALE + self.player.rect[0] +
      (int(self.player.hit_timer > 0) * GET_HIT_PENALTY) +
      self.actionPenalty + stallPenalty + timePenalty)
      
      
   def getScoreTheta(self):
      return self.currentScore - self.lastScore
      
      
   def updateScore(self):
      #self.longLastScore = self.lastScore
      self.lastScore = self.currentScore
      self.currentScore = self.getHeuristicScore()
      
      for x in range(1, STALL_MEMORY):
         self.posXHistory[0][x - 1] = self.posXHistory[0][x]
      self.posXHistory[0][STALL_MEMORY - 1] = self.player.rect[0]
      
      
   def evaluate(self):
      #pprint(self.inputData)
      #pprint(self.outputData)
      
      self.model.fit(self.inputData, self.outputData, nb_epoch=EPOCH, batch_size=BATCH_SIZE)
      self.updateScore()
      
      if(self.frameCount > 0):
         if self.getScoreTheta() > 0:
            # it did good, fit to itself
            self.outputData[0][0] = self.outputData[0][0]
            self.outputData[0][1] = self.outputData[0][1]
         else:
            # if was wrong then randomize the direction to fit, but weight toward right
            choice = numpy.random.randint(0,6)
            self.outputData[0][0] = (bot.DIR_LEFT if choice == 0 else (bot.DIR_STILL if choice == 1 else bot.DIR_RIGHT))
            # and whether or not to jump but weight toward not
            choice = numpy.random.randint(0,10)
            self.outputData[0][1] = 0 if choice == 0 else 1
      else:
         self.outputData[0][0] = 1
         self.outputData[0][1] = 0 #default to doing the right thing at the start
         
      self.evaluation = self.model.predict(self.inputData, batch_size=BATCH_SIZE, verbose=0)


   # gets the offset position of a tile to put in the tile array fed to the NN
   def getLocalTileGridPosition(self, entity):
      gridPos = [0, 0]
      gridPos[0] = int(round((entity.pos[0] - self.player.rect[0]) / bot_math.TILE_SIZE)) + TILE_MAX_X_DIST + TILE_X_OFFSET
      gridPos[1] = int(round((entity.pos[1] - self.player.rect[1]) / bot_math.TILE_SIZE)) + TILE_MAX_Y_DIST + TILE_Y_OFFSET
      return gridPos
      
      
   def inputLevelStateData(self, entities):
      self.inputData = numpy.zeros((1, 142))
      currentEnemyCount = 0
      
      self.inputData[0][PLAYER_FEATURE_OFFSET] = int(self.player.jumping)
      self.inputData[0][PLAYER_FEATURE_OFFSET + 1] = self.player.rect[0]
      self.inputData[0][PLAYER_FEATURE_OFFSET + 2] = self.player.rect[1]
      self.inputData[0][PLAYER_FEATURE_OFFSET + 3] = self.player.angle
      self.inputData[0][PLAYER_FEATURE_OFFSET + 4] = self.player.hp
      self.inputData[0][PLAYER_FEATURE_OFFSET + 5] = self.player.hit_timer
      
      # set 1 or 0 for each tile in array
      for e in entities:
         gridPos = self.getLocalTileGridPosition(e)
         # is an enemy
         if(bot_math.isHazard(e) and 
         bot_math.getRelativeDistance((self.player.rect[0], self.player.rect[1]), e) < bot_math.TILE_SIZE * ENEMY_TILE_CHECK_DIST and
         currentEnemyCount < MAX_ENEMY_COUNT):
            #pprint (vars(e))
            #print("ID: " + str(bot_math.getHazardID(e)))
            self.inputData[0][ENEMY_FEATURE_OFFSET + currentEnemyCount * SINGLE_ENEMY_FEATURES] = bot_math.getHazardID(e)
            self.inputData[0][ENEMY_FEATURE_OFFSET + currentEnemyCount * SINGLE_ENEMY_FEATURES + 1] = e.pos[0]
            self.inputData[0][ENEMY_FEATURE_OFFSET + currentEnemyCount * SINGLE_ENEMY_FEATURES + 2] = e.pos[1]
            self.inputData[0][ENEMY_FEATURE_OFFSET + currentEnemyCount * SINGLE_ENEMY_FEATURES + 3] = e.speed
            currentEnemyCount += 1
         # is a tile or non-damaging level object (not a Baddie)
         elif (gridPos[0] > -1 and gridPos[0] < TILE_MAX_X_DIST * 2 and gridPos[1] > -1 and gridPos[1] < TILE_MAX_Y_DIST * 2):
            self.inputData[0][TILE_FEATURE_OFFSET + gridPos[0] + gridPos[1] * TILE_MAX_Y_DIST * 2] = 1
            

      #if self.frameCount % 5 == 0: # DEBUG PRINT
      #   print(self.inputData)
#end of NeuralNetworkBot class

# for storage of references and model
neural_network_bot = NeuralNetworkBot()
waitToRestart = None
hasSaved = False

# initialize the neural network model
def init_bot(player):
   print("Initializing NN")
   global waitToRestart
   global hasSaved
   seed = 7
   numpy.random.seed(seed)
   neural_network_bot.setPlayerRef(player)
   neural_network_bot.initModel()
   neural_network_bot.lastDecision = (bot.DIR_RIGHT, False)
   neural_network_bot.actionPenalty = 0
   neural_network_bot.isStalled = False
   neural_network_bot.posXHistory = numpy.zeros((1, STALL_MEMORY))
   neural_network_bot.posXHistory[0][STALL_MEMORY - 1] = -1
   waitToRestart = False
   hasSaved = False
   
# called by the game once per frame
def getDecision(state):
   global waitToRestart
   global hasSaved

   if neural_network_bot.frameCount % FRAME_SKIP == 0 and waitToRestart == False:
      neural_network_bot.inputLevelStateData(state.entities)
      neural_network_bot.evaluate()
      neural_network_bot.updateIsStalled()
      neural_network_bot.lastDecision = (getDirection(state), getJumping(state))
      hasSaved = False
      
      print("SCORE: " + str(neural_network_bot.getHeuristicScore()))
      if neural_network_bot.isStalled:
         neural_network_bot.player.kill()
         waitToRestart = True
      
   if neural_network_bot.frameCount % NN_MODEL_SAVE_SKIP == 0 or waitToRestart == True:
      if hasSaved == False:
         print("Saving NN model to file")
         neural_network_bot.model.save(NN_MODEL_PATH)
         print("Save of NN model complete")
         hasSaved = True
      
   neural_network_bot.frameCount += 1
   return neural_network_bot.lastDecision

def getDirection(state):
   print("DIRECTION OUTPUT: " + str(neural_network_bot.evaluation[0][0]))
   
   direction = bot.DIR_STILL
   if neural_network_bot.evaluation[0][0] > 0.001:
      direction = bot.DIR_RIGHT
   if neural_network_bot.evaluation[0][0] < -0.001:
      direction = bot.DIR_LEFT
   
   return direction
    
def getJumping(state):
   print("JUMP OUTPUT: " + str(neural_network_bot.evaluation[0][1]))
   
   return neural_network_bot.evaluation[0][1] > 0
   
