from bots import bot
from bots.game_state import *
from bots import bot_math
from gamelib import sprites

# called by the game once per frame
def getDecision(state):
    dec = (getDirection(state), getJumping(state))
    return dec


def getJumping(state):
    jump_entities = [sprites.Baddie, sprites.Pipe, sprites.PipeBig, sprites.AirPlatform, sprites.AirPlatformblue, sprites.PipeGreen, sprites.PipeGreenBig]
    for e in state.entities:
        dx = e.pos[0] - state.player[0]
        dy = abs(e.pos[1] - state.player[1])
        if (e.obj_type in jump_entities or type(e.obj_type) is str) and abs(dx) < 70 and dy <= 32 * 3:
            if type(e.obj_type) is str:
                return True
            if dx > 0:
                return True            

    x = int(state.player[0] / 32) + 1
    y = int(430 / 32)
    # jump if there is a hole in front of us
    tile = getAt(state, x, y)
    if tile in [(254, 254, 255, 255), (255, 255, 255, 255)]:
        return True
    return False

counter = 0

def getDirection(state):
    global counter
    if counter > 0:
        counter = counter - 1
        return bot.DIR_LEFT

    player = state.player
    x = int(state.player[0] / 32)
    y = int(state.player[1] / 32)
    close = closeEntities(state.player, state.entities, 32 * 3)

    ceilings = [sprites.Brickblue]
    ceiling_positions = [(int(e.pos[0] / 32), int(e.pos[1] / 32)) for e in close if e.obj_type in ceilings]

    close_positions = [(int(e.pos[0] / 32), int(e.pos[1] / 32)) for e in close]

    for e in close:
        diff = e.pos[0] - state.player[0]
        # check if the ceiling above us is obstructed
        if ((x, y - 1 ) in ceiling_positions or (x + 1, y - 1) in ceiling_positions) and type(e.obj_type) is str and diff > 0 and diff < 70:
            counter = 50
            return bot.DIR_LEFT
    return bot.DIR_RIGHT
             
def closeEntities(player, entities, distance):
    return [e for e in entities if abs(e.pos[0] - player[0]) < distance and abs(e.pos[1] - player[1]) < distance]

def lookAhead(state):
    x = int(state.player[0] / 32) + 1
    y = int(430 / 32) - 1
    return getAt(state, x, y)

def lookAheadGround(state):
    x = int(state.player[0] / 32) + 2
    y = int(430 / 32)
    return getAt(state, x, y)

def getAt(state, x, y):
    try:
        return state.level.level.get_at((x, y))
    except:
        pass

def getClose(state, dx, dy):
    x = int(state.player[0] / 32) + dx
    # ground level
    y = int(430 / 32) - 1 + dy
    return getAt(state, x, y)
   
# Platform
# (0, 0, 0, 0)

# Platformblue   
# (102, 124, 255, 255)

# AirPlatformblue
# (192, 192, 192, 255)
